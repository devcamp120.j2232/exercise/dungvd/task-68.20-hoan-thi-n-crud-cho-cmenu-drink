-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 26, 2023 lúc 04:45 PM
-- Phiên bản máy phục vụ: 10.4.28-MariaDB
-- Phiên bản PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `pizza_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `don_gia` bigint(20) NOT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL,
  `ma_nuoc_uong` varchar(255) NOT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Đang đổ dữ liệu cho bảng `drinks`
--

INSERT INTO `drinks` (`id`, `don_gia`, `ghi_chu`, `ma_nuoc_uong`, `ngay_cap_nhat`, `ngay_tao`, `ten_nuoc_uong`) VALUES
(14, 10000, NULL, 'TRATAC', NULL, '2023-04-26 21:42:10', 'Trà tắc'),
(15, 15000, NULL, 'COCA', NULL, '2023-04-26 21:42:28', 'Cocacola'),
(16, 15000, NULL, 'PEPSI', NULL, '2023-04-26 21:42:36', 'Pepsi'),
(17, 10000, NULL, 'LAVIE', NULL, '2023-04-26 21:42:57', 'Lavie'),
(18, 40000, NULL, 'TRASUA', NULL, '2023-04-26 21:43:06', 'Trà sữa trân châu'),
(19, 15000, NULL, 'FANTAAA', '2023-04-26 21:43:50', '2023-04-26 21:43:12', 'Fanta');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(20);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menuss`
--

CREATE TABLE `menuss` (
  `id` bigint(20) NOT NULL,
  `duong_kinh` varchar(255) NOT NULL,
  `kich_co` varchar(255) NOT NULL,
  `salad` bigint(20) NOT NULL,
  `so_luong_nuoc` int(11) NOT NULL,
  `suon` bigint(20) NOT NULL,
  `thanh_tien` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Đang đổ dữ liệu cho bảng `menuss`
--

INSERT INTO `menuss` (`id`, `duong_kinh`, `kich_co`, `salad`, `so_luong_nuoc`, `suon`, `thanh_tien`) VALUES
(8, '20', 'S', 200, 2, 2, 150000),
(9, '25', 'M', 300, 3, 4, 200000),
(10, '30', 'L', 500, 4, 8, 250000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `order_code` varchar(255) NOT NULL,
  `paid` bigint(20) DEFAULT NULL,
  `pizza_size` varchar(255) NOT NULL,
  `pizza_type` varchar(255) NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `ngay_cap_nhat`, `ngay_tao`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `voucher_code`, `order_id`) VALUES
(4, NULL, '2023-04-26 20:06:20', 'anxb', 150000, 'S', 'Hai San', 15000, NULL, 3),
(5, NULL, '2023-04-26 20:06:51', 'zzaw', 200000, 'M', 'Bacon', 200000, NULL, 1),
(6, NULL, '2023-04-26 20:09:18', 'sa2', 250000, 'L', 'Hawaii', 200000, '12354', 2),
(7, NULL, '2023-04-26 20:17:36', 'sa2', 250000, 'L', 'Hawaii', 200000, '12354', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `address`, `email`, `fullname`, `ngay_cap_nhat`, `ngay_tao`, `phone`) VALUES
(1, 'An Phu', 'duydung@gamil.com', 'dung vu duy', NULL, '2023-04-26 19:57:34', '0987541241'),
(2, 'HCM', 'nhattaondinh@gamil.com', 'dinh nhat taon', NULL, '2023-04-26 19:58:04', '0987232'),
(3, 'Tan Uyennn', 'thanhnhan@gamil.commm', 'thanh nhannnn', '2023-04-26 20:03:59', '2023-04-26 20:03:10', '096531244');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ioautl9bhwm9nmdgersk8mjx` (`ma_nuoc_uong`),
  ADD UNIQUE KEY `UK_nvfpgsg41yfn7kycuqr5b7i82` (`ten_nuoc_uong`);

--
-- Chỉ mục cho bảng `menuss`
--
ALTER TABLE `menuss`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKh0vxwit7yrip7v9pjqlocmbxp` (`order_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKh0vxwit7yrip7v9pjqlocmbxp` FOREIGN KEY (`order_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
