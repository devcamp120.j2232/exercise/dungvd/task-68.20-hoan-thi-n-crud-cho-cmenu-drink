package com.devcamp.cruddrinks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menuss")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "kich_co" , nullable = false, updatable = true)
    private String kichCo;

    @Column(name = "duong_kinh" , nullable = false, updatable = true)
    private String duongKinh;

    @Column(name = "suon", nullable = false, updatable = true)
    private long suon;

    @Column(name = "salad", nullable = false, updatable = true)
    private long salad;

    @Column(name = "so_luong_nuoc", nullable = false, updatable = true)
    private int soLuongNuoc;

    @Column(name = "thanh_tien", nullable = false, updatable = true)
    private long thanhTien;

    public CMenu(String kichCo, String duongKinh, long suon, long salad, int soLuongNuoc, long thanhTien) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuoc = soLuongNuoc;
        this.thanhTien = thanhTien;
    }

    public CMenu() {
    }

    public String getKichCo() {
        return kichCo;
    }

    public void setKichCo(String kichCo) {
        this.kichCo = kichCo;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public long getSuon() {
        return suon;
    }

    public void setSuon(long suon) {
        this.suon = suon;
    }

    public long getSalad() {
        return salad;
    }

    public void setSalad(long salad) {
        this.salad = salad;
    }

    public int getSoLuongNuoc() {
        return soLuongNuoc;
    }

    public void setSoLuongNuoc(int soLuongNuoc) {
        this.soLuongNuoc = soLuongNuoc;
    }

    public long getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(long thanhTien) {
        this.thanhTien = thanhTien;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
}
