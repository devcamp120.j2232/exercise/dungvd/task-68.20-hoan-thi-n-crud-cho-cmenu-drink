package com.devcamp.cruddrinks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cruddrinks.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long>{
    
}
