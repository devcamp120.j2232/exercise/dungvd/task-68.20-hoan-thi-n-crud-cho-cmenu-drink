package com.devcamp.cruddrinks.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.cruddrinks.model.CDrink;
import com.devcamp.cruddrinks.repository.IDrinkRepository;

@RestController
@CrossOrigin
public class CDrinkController {
    @Autowired
    IDrinkRepository pIDrinkRepository;

    // API lấy danh sách toàn bộ nước uống
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            // Tạo 1 list để lưu danh sách drinks
            List<CDrink> listAllDrinks = new ArrayList<>();
            // Truy xuất trong database và add vào biến thông qua hàm findAll và foreach
            pIDrinkRepository.findAll().forEach(listAllDrinks::add);
            // Trả về list danh sách nước uống và status ok
            return new ResponseEntity<>(listAllDrinks, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi print lỗi ra terminal và trả về status lỗi
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API lấy thông tin nước uống bằng ID
    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id) {
        try {
            // tìm kiếm 1 optional<Drink> thông qua Id thông qua hàm findById từ Class repository và gán vào biển drink
            Optional<CDrink> drink = pIDrinkRepository.findById(id);
            // kiểm tra biến drink có chứa giá trị hay không
            if(drink.isPresent()) {
                // Nếu có return về kiểu CDrink qua hàm .get() và status ok
                return new ResponseEntity<>(drink.get(), HttpStatus.OK);
            }
            else {
                // Nếu không trả về status not found
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // nếu gặp lỗi in lỗi ra terminal và ghi status lỗi ra
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để tạo mới 1 loại nước uống
    @PostMapping("/drinks")
    public ResponseEntity<CDrink> createDrink(@Valid @RequestBody CDrink pDrink) {
        try {
            // Set ngày tạo và ngày cập nhật cho loại nước uống mới thêm vào
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            // dùng biến _drink kiểu dữ liệu CDrink để lưu giá trị vừa tạo
            CDrink _drink = pIDrinkRepository.save(pDrink);
            // Return loại nước vừa tạo và status ok
            return new ResponseEntity<>(_drink, HttpStatus.OK);
        } catch (Exception e) {
            // nếu gặp lỗi in lỗi ra terminal và ghi status lỗi ra
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // API để sửa thông tin 1 loại nước uống thông qua id
    @PutMapping("drinks/{id}")
    public ResponseEntity<CDrink> updateDrink(@PathVariable("id") long id, @RequestBody CDrink pDrink) {
        try {
            // Dùng hàm findById từ repository để get Giá trị thông qua ID chứa trong 1 biến drinkData kiểu dữ liệu Optional<CDrink>
            Optional<CDrink> drinkData = pIDrinkRepository.findById(id);
            // Kiểm tra xem có tồn tại drink hay không qua hàm isPresent
            if(drinkData.isPresent()) {
                // Trả về kiểu dữ liệu CDrink thông qua hàm .get();
                CDrink drink = drinkData.get();
                // Cập nhật lại giá trị và ngày cập nhật
                drink.setMaNuocUong(pDrink.getMaNuocUong());
                drink.setTenNuocUong(pDrink.getTenNuocUong());
                drink.setDonGia(pDrink.getDonGia());
                drink.setGhiChu(pDrink.getGhiChu());
                drink.setNgayCapNhat(new Date());
                // lưu thông tin đã cập nhật thông qua biến _drink và hàm save từ repository
                CDrink _drink = pIDrinkRepository.save(drink);
                // trả về giá trị mới và status ok
                return new ResponseEntity<>(_drink, HttpStatus.OK);
            }
            else {
                // nếu không trả về status not found
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // nếu gặp lỗi in lỗi ra terminal và ghi status lỗi ra
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // API delete 1 loại nước uống thông qua id
    @DeleteMapping("drinks/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id) {
        try {
            // Xóa thông qua hàm deleteById()
            pIDrinkRepository.deleteById(id);
            // Trả về status no content
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // nếu gặp lỗi in lỗi ra terminal và ghi status lỗi ra
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
