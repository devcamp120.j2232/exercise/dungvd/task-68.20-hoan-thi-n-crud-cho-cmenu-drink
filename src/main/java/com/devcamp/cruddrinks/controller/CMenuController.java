package com.devcamp.cruddrinks.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.cruddrinks.model.CMenu;
import com.devcamp.cruddrinks.repository.IMenuRepository;


@RestController
@CrossOrigin
public class CMenuController {
    @Autowired
    IMenuRepository pIMenuRepository;

    @GetMapping("/menus")
    public ResponseEntity<List<CMenu>> getAllMenu() {
        try {
            List<CMenu> listMenu = new ArrayList<>();
            pIMenuRepository.findAll().forEach(listMenu::add);
            return new ResponseEntity<>(listMenu, HttpStatus.OK);           
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);           

        }
    }



    // API để get 1 menu thông qua Id
    @GetMapping("/menu/{id}")
    public ResponseEntity<CMenu> getMenuById(@PathVariable("id") long id) {
        try {
            CMenu menu = pIMenuRepository.findById(id).get();
            return new ResponseEntity<>(menu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);           
        }
    }


    // API để tạo mới 1 menu
    @PostMapping("menus") 
    public ResponseEntity<CMenu> createNewMenu(@RequestBody CMenu pMenu) {
        try {
            return new ResponseEntity<>(pIMenuRepository.save(pMenu), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);           
        }
    }

    // API để sửa 1 menu
    @PutMapping("/menu/{id}")
    public ResponseEntity<CMenu> updateMenu(@RequestBody CMenu pMenu, @PathVariable("id") long id) {
        try {
            Optional<CMenu> menuData = pIMenuRepository.findById(id);
            if(menuData.isPresent()) {
                CMenu menu = menuData.get();
                menu.setDuongKinh(pMenu.getDuongKinh());
                menu.setKichCo(pMenu.getKichCo());
                menu.setSalad(pMenu.getSalad());
                menu.setSuon(pMenu.getSuon());
                menu.setSoLuongNuoc(pMenu.getSoLuongNuoc());
                menu.setThanhTien(pMenu.getThanhTien());
                CMenu menuSaved = pIMenuRepository.save(menu);
                return new ResponseEntity<>(pIMenuRepository.save(menuSaved), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);           
        }
    }


    //API để xóa 1 menu
    @DeleteMapping("/menu/{id}")
    public ResponseEntity<CMenu> deleteMenu(@PathVariable("id") long id) {
        try {
            pIMenuRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);           
        }
    }
}
